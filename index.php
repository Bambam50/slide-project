<!doctype html>
<head><title> campaign lead</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/camp.css">
<link rel="stylesheet" type="fold/js/bootstrap.min.js">
</head>

    <style>
  * {box-sizing:border-box}
  
  /* Slideshow container */
  .slideshow-container {
    max-width: 1000px;
    position: relative;
    margin: auto;
  }
  
  /* Hide the images by default */
  .mySlides {
    display: none;
  }
  
  /* Next & previous buttons */
  .prev, .next {
    cursor: pointer;
    position: absolute;
    top: 50%;
    width: auto;
    margin-top: -22px;
    padding: 16px;
    color: white;
    font-weight: bold;
    font-size: 18px;
    transition: 0.6s ease;
    border-radius: 0 3px 3px 0;
    user-select: none;
  }
  
  /* Position the "next button" to the right */
  .next {
    right: 0;
    border-radius: 3px 0 0 3px;
  }
  
  /* On hover, add a black background color with a little bit see-through */
  .prev:hover, .next:hover {
    background-color: rgba(0,0,0,0.8);
  }
  
  /* Caption text */
  .text {
    color: #4b0505;;
    font-size:30px;
    padding: 8px 12px;
    position: absolute;
    bottom: 120px;
    width: 100%;
    text-align:center;
    text-shadow:0 0 6px red;
    
  }
  
  /* Number text (1/3 etc) */
  .numbertext {
    color:white;
    font-size: 12px;
    text:bold;
    padding: 8px 12px;
    position: absolute;
    top: 0;
  }
  
  /* The dots/bullets/indicators */
  .dot {
    cursor: pointer;
    height: 15px;
    width: 15px;
    margin: 0 2px;
    background-color: #bbb;
    border-radius: 50%;
    display: inline-block;
    transition: background-color 0.6s ease;
  }
  
  .active, .dot:hover {
    background-color: #717171;
  }
  
  /* Fading animation */
  .fade {
    animation-name: fade;
    animation-duration: 1.5s;
  }
  
  @keyframes fade {
    from {opacity: .4}
    to {opacity: 1}
  }
      </style>
<html>
  <body>
<!-- Slideshow container -->
<div class="slideshow-container">
  <?php 
    $data = file_get_contents('main.json');
    $data = json_decode($data);
    $count = count($data);
    $valid=date('d-m-y h:i:s')
  ?>

  <?php 
    $no = 1;
    foreach($data as $data){
      ?>
    <!-- Full-width images with number and caption text -->
    <div class="mySlides fade">
      <div class="numbertext"><?php echo $no;?> / <?php echo $count;?></div>
      <img src="image/<?php echo $data->image; ?>" style="width:100%">
      <div class="text" style="text-center"><?php echo $data->name;?><br>
      <h5> <?php echo $data->act;?></h5>
    <span><?php echo $valid;?></span></div>

    </div>
  
  <?php
    $no++;
    }
  ?>
   
  
    <!-- Next and previous buttons -->
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>
  </div>
  <br>
  
  <!-- The dots/circles -->
  <div style="text-align:center">
  <?php 
    $data = file_get_contents('main.json');
    $data = json_decode($data);
  ?>
    <?php 
      $no = 1;
      foreach($data as $data){
    ?>
      <span class="dot" onclick="currentSlide(<?php echo $no;?>)"></span>
    <?php
    $no++;
      }
    ?>

  </div>
  <script>
  
 
  
  let slideIndex = 0;
  showSlides();
  
  function showSlides() {
    let i;
    let slides = document.getElementsByClassName("mySlides");
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex > slides.length) {slideIndex = 1}
    slides[slideIndex-1].style.display = "block";
    setTimeout(showSlides, 2000); // Change image every 2 seconds
  }
  </script>
  </body>
</html>